var express = require('express');
var router = express.Router();
var exec = require('child_process').exec;
var fs = require('fs');
var path = require('path');
var configuration = require('../configuration.json');

router.get('/', function(req, res) {
  res.send("PRINTER SERVER IS READY!");
});

router.post('/print/raw', function(req, res) {  
  var content = req.body.content;
  var filename = path.join(".", "print-logs", Date.now().toString());
  // console.log(configuration)
  var printerName = "\\\\" + configuration['computer-name'] + "\\" + configuration['printer-name'];

  fs.writeFile(filename, content, function(err) {
    if(err) {
      return res.status(400).end();
    }

    var cmd = 'copy /b ' + filename + ' "' + printerName + '"';
    console.log(cmd);
    exec(cmd, function(err, out, code) {
      if (err instanceof Error) {
        if (err) console.log(err);
        return res.status(400).json(err);
      }
      return res.json({success:true});
    });
  }); 
});

module.exports = router;
